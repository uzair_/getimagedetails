#! /usr/bin/python
import os
from PIL import Image
import csv
import scandir

class ImageDetails:
    def __init__(self):
        self.i = 0
        
    def getDetails(self):
        with open("imageDetails.csv", "w") as csvfile:
            wrt = csv.writer(csvfile,delimiter=";")
            getList = self.getList()
            for filename in getList:
                print filename
                try:
                    img = Image.open("images/"+filename)
                    height, width = img.size
                    wrt.writerow([filename, width, height, (width*height*3)/1048576])
                except IOError:
                    pass
                
    def getList(self):
        files = scandir.scandir("images")
        for filename in files:
            yield filename.name
    

ID = ImageDetails()
ID.getDetails()