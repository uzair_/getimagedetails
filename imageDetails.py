#! /usr/bin/python
import xlwt
import os
from PIL import Image

class ImageDetails:
    def __init__(self):
        self.i = 0
        
    def getDetails(self):
        files = os.listdir("images")
        book = xlwt.Workbook()
        sheet = book.add_sheet("Image Details")
        for filename in files:
            print filename
            try:
                img = Image.open("images/"+filename)
                pix = img.load()
                height, width = img.size
                x = max(img.size)
                sheet.write(self.i,0,filename)
                sheet.write(self.i,1,width)
                sheet.write(self.i,2,height)
                sheet.write(self.i,3,(width*height*3)/1048576)
                self.i+=1
            except IOError:
                sheet.write(self.i,0,filename)
                sheet.write(self.i,1, "This is corrupted")
                self.i+=1
        book.save("images_details.xls")
        

ID = ImageDetails()
ID.getDetails()